﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EncodeToy.Models
{
    /// <summary>
    /// Interface for named encoder factories to implement.
    /// This allows creating factories which can be identified by string name.
    /// </summary>
    public interface INamedEncoderFactory {
        /// <summary>
        /// Name of the encoder.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Factory method.
        /// </summary>
        /// <returns>New instance of encoder.</returns>
        IEncoder Create();
    }

    /// <summary>
    /// Encoder/Decoder.
    /// </summary>
    public interface IEncoder {
        /// <summary>
        /// Encodes open text.
        /// </summary>
        /// <param name="openText">Text to be encoded.</param>
        /// <returns>Result of the encoding process.</returns>
        EncodingResult Encode(string openText);
        /// <summary>
        /// Decodes closed text.
        /// </summary>
        /// <param name="closedText">Text to be decoded.</param>
        /// <returns>Result of the decoding process.</returns>
        EncodingResult Decode(string closedText);
    }
}
