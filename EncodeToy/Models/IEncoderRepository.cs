﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EncodeToy.Models
{
    /// <summary>
    /// Interface for repositories of encoders implement to.
    /// </summary>
    public interface IEncoderRepository {
        /// <summary>
        /// Gets new encoder with given name.
        /// </summary>
        /// <param name="name">Name of the encoder.</param>
        /// <returns>New encoder instance.</returns>
        IEncoder GetEncoder(string name);

        /// <summary>
        /// Enumeration of all available encoders.
        /// </summary>
        IEnumerable<string> Names { get; }
    }
}
