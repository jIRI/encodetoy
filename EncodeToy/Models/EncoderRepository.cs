﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EncodeToy.Models {
    /// <summary>
    /// Repository of encoders implementing IEncoder interface.
    /// </summary>
    public class EncoderRepository : IEncoderRepository {
        private IDictionary<string, INamedEncoderFactory> _encoderFactoriesMap = new Dictionary<string, INamedEncoderFactory>();

        /// <summary>
        /// ctor.
        /// </summary>
        /// <param name="encoderFactories">Enumeration of existing encoders implementing IEncoder interface. Populated by DI container; see settings in Startup class.</param>
        public EncoderRepository(IEnumerable<INamedEncoderFactory> encoderFactories) {
            foreach (var fact in encoderFactories) {
                this._encoderFactoriesMap.Add(fact.Name, fact);
            }
        }

        public IEncoder GetEncoder(string name) {
            if (string.IsNullOrEmpty(name) || !this._encoderFactoriesMap.ContainsKey(name)) {
                return null;
            }

            return this._encoderFactoriesMap[name].Create();
        }

        public IEnumerable<string> Names {
            get {
                return this._encoderFactoriesMap.Keys;
            }
        }
    }
}
