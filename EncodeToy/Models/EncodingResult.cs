﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EncodeToy.Models {
    /// <summary>
    /// Result of the encoding process.
    /// </summary>
    public enum Result {
        OK,
        Error
    }

    /// <summary>
    /// Result of the encoding or decoding process.
    /// </summary>
    public class EncodingResult {
        public static readonly EncodingResult NoSuchEncoder = new EncodingResult(Result.Error, "No such encoder.", null);

        /// <summary>
        /// ctor handling successfull encoding/decoding.
        /// </summary>
        /// <param name="data">Output of the en/decoding.</param>
        public EncodingResult(string data) : this(Result.OK, null, data) {
        }

        /// <summary>
        /// ctor.
        /// </summary>
        /// <param name="result">Result of the en/decoding process.</param>
        /// <param name="errorMessage">Error which occured during en/decoding if there was one; otherwise null.</param>
        /// <param name="data">Output of the en/decoding.</param>
        public EncodingResult(Result result, string errorMessage, string data) {
            this.Result = result;
            this.ErrorMessage = errorMessage;
            this.Data = data;
        }

        /// <summary>
        /// Result of the en/decoding process.
        /// </summary>
        public Result Result { get; private set; }

        /// <summary>
        /// Error which occured during en/decoding if there was one; otherwise null.
        /// </summary>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Output of the en/decoding.
        /// </summary>
        public string Data { get; private set; }
    }
}
