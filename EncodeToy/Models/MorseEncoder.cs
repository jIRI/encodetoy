﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncodeToy.Models {
    /// <summary>
    /// Morse code encoder named factory class.
    /// </summary>
    public class MorseEncoderFactory : INamedEncoderFactory {
        public string Name => "morse";

        public IEncoder Create() {
            return new MorseEncoder();
        }
    }

    /// <summary>
    /// Morse code encoder.
    /// </summary>
    public class MorseEncoder : IEncoder {
        private const char SEPARATOR = '/';

        private static readonly Dictionary<char, string> _encodeMap = new Dictionary<char, string> {
            // letters
            ['A'] = ".-", ['B'] = "-...", ['C'] = "-.-.", ['D'] = "-..", ['E'] = ".", ['F'] = "..-.",
            ['G'] = "--.", ['H'] = "....", ['I'] = "..", ['J'] = ".---", ['K'] = "-.-", ['L'] = ".-..",
            ['M'] = "--", ['N'] = "-.", ['O'] = "---", ['P'] = ".--.", ['Q'] = "--.-", ['R'] = ".-.",
            ['S'] = "...", ['T'] = "-", ['U'] = "..-", ['V'] = "...-", ['W'] = ".--", ['X'] = "-..-",
            ['Y'] = "-.--", ['Z'] = "--..",
            // numbers
            ['1'] = ".----", ['2'] = "..---", ['3'] = "...--", ['4'] = "....-", ['5'] = ".....", ['6'] = "-....",
            ['7'] = "--...", ['8'] = "---..", ['9'] = "----.", ['0'] = "-----",
            // special chars
            ['?'] = "..--..", [','] = "--..--", ['!'] = "--...-", ['.'] = ".-.-.-", [';'] = "-.-.-.", ['/'] = "-..-.",
            ['='] = "-...-", ['-'] = "-....-", ['\t'] = ".----.", ['('] = "-.--.", [')'] = "-.--.-", ['"'] = ".-..-.",
            [':'] = "---...", ['_'] = "..--.-", ['@'] = ".--.-.",
        };
        private static readonly Dictionary<string, char> _decodeMap = new Dictionary<string, char>();


        static MorseEncoder() {
            // build decode map
            foreach(var key in MorseEncoder._encodeMap.Keys) {
                _decodeMap.Add(MorseEncoder._encodeMap[key], key);
            }            
        }


        public EncodingResult Decode(string closedText)
        {
            if (string.IsNullOrEmpty(closedText)) {
                return new EncodingResult(Result.Error, "Closed text is empty.", closedText);
            }

            // split by separator, translate
            var charStrings = closedText.Split(MorseEncoder.SEPARATOR);
            var resultBuilder = new StringBuilder(charStrings.Length);
            foreach (var charStr in charStrings) {
                if (MorseEncoder._decodeMap.ContainsKey(charStr)) {
                    resultBuilder.Append(MorseEncoder._decodeMap[charStr]);
                } else {
                    resultBuilder.Append("#");                
                }
            }
            return new EncodingResult(resultBuilder.ToString());
        }

        public EncodingResult Encode(string openText) {
            if (string.IsNullOrEmpty(openText)) {
                return new EncodingResult(Result.Error, "Open text is empty.", openText);
            }

            // set capacity to kind of worse case scenario
            var resultBuilder = new StringBuilder(openText.Length * 5);
            var charCount = openText.Length;
            foreach (var c in openText.ToUpperInvariant()) {
                if (MorseEncoder._encodeMap.ContainsKey(c)) {
                    resultBuilder.Append(MorseEncoder._encodeMap[c]);
                } else {
                    resultBuilder.Append("#");
                }
                charCount--;
                if (charCount > 0) {
                    resultBuilder.Append(MorseEncoder.SEPARATOR);
                }
            }

            return new EncodingResult(resultBuilder.ToString());
        }
    }
}

