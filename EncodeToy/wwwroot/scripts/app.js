﻿//
// Encodes given open text using given encoder and sets result to UI.
//
function encodeText(encoder, openText) {
    $.getJSON('/api/encode', {
        encoder: encoder,
        openText: openText
    })
        .done(function (response) {
            setResult(response);
        })
        .fail(function (error) {
            setResult("Error while trying to convert open text to closed text: " + error.responseText);
        });
}

//
// Decodes given closed text using given encoder and sets result to UI.
//
function decodeText(encoder, closedText) {
    $.getJSON('/api/decode', {
        encoder: encoder,
        closedText: closedText
    })
        .done(function (response) {
            setResult(response);
        })
        .fail(function(error) {
            setResult("Error while trying to convert closed text to open text: " + error.responseText);
        });
}

//
// Applies given result object to result part of UI.
//
function setResult(result) {
    if (result.status === 'OK') {
        $('#result').text(result.result);
    } else {
        $('#result').text(result.error || 'Unknown error occured while processing encode/decode request.');
    }
}

//
// Gets text from source text area.
//
function getSource() {
    return $('#source').val();
}

//
// Gets name of the selected encoder.
//
function getEncoder() {
    return $('#encoders').val();
}

//
// Requests list of encoders and populates UI dropdown.
//
function populateEncoders() {
    $.getJSON('/api/encoders')
        .done(function (response) {
            var select = $('#encoders');
            select.html('');
            if (response.encoders != null) {
                response.encoders.forEach(function (e, i) {
                    select.append('<option value="' + e + '">' + e + '</option>');
                    if (i === 0) {
                        select.val(e);
                    }
                });
            } else {
                select.append('ERROR: Unable to read encoders list.');
            }
        })
        .fail(function (error) {
            setResult("Error while trying to convert open text to closed text: " + error.responseText);
        });      
}
