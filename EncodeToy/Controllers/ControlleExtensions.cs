﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EncodeToy.Controllers.Extensions {
    public static class ControllerExtensions {
        /// <summary>
        /// Converts EncodingResult to JSON.
        /// </summary>
        /// <param name="result">Object to be converted.</param>
        /// <returns>JSON representation of the EncodingResult.</returns>
        public static JsonResult ToJson(this EncodeToy.Models.EncodingResult result) {
            return new JsonResult(new {
                status = result.Result.ToString(),
                result = result.Data,
                error = result.ErrorMessage
            });
        }
    }
}
