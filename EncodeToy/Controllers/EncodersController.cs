﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EncodeToy.Controllers.Extensions;
using EncodeToy.Models;

namespace EncodeToy.Controllers {

    [Route("api/[controller]")]
    public class EncodersController : Controller {
        private IEncoderRepository _repo;

        public EncodersController(IEncoderRepository repo) {
            this._repo = repo;
        }

        /// <summary>
        /// Returns list of available encoders.
        /// </summary>
        /// <returns>JSON objects with following format:
        /// {
        ///     encoders: ['encoder1', 'encoder2']
        /// }
        /// </returns>
        [HttpGet]
        public JsonResult GetAll() {
            return Json(new { encoders = _repo.Names });
        }
    }
}
