﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EncodeToy.Models;
using EncodeToy.Controllers.Extensions;

namespace EncodeToy.Controllers {

    [Route("api/[controller]")]
    public class DecodeController : Controller {
        private IEncoderRepository _repo;
        public DecodeController(IEncoderRepository repo) {
            this._repo = repo;
        }

        /// <summary>
        /// Responds to HTTP GET by converting given closedText using selected encoder.
        /// 
        /// Expected request parameters:
        /// encoder: Name of the encoder.
        /// closedText: Text to be decoded.
        /// </summary>
        /// <returns>JSON representation of decoding result. See ControllerExtensions.ToJson() for response details.</returns>
        [HttpGet]
        public JsonResult GetAll() {
            var encoderId = HttpContext.Request.Query["encoder"];
            var encoder = this._repo.GetEncoder(encoderId);
            if (encoder != null) {
                var closedText = HttpContext.Request.Query["closedText"];
                var encodeResult = encoder.Decode(closedText);
                return encodeResult.ToJson();
            } else {
                return EncodingResult.NoSuchEncoder.ToJson();
            }
        }
    }
}
