﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EncodeToy.Controllers {
    public class HomeController : Controller {
        /// <summary>
        /// Handles Index get request.
        /// </summary>
        /// <returns>Index view.</returns>
        public IActionResult Index() {
            return View();
        }

        /// <summary>
        /// Handles Error.
        /// </summary>
        /// <returns>Error view.</returns>
        public IActionResult Error() {
            return View();
        }
    }
}
