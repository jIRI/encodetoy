﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EncodeToy.Controllers.Extensions;
using EncodeToy.Models;

namespace EncodeToy.Controllers {

    [Route("api/[controller]")]
    public class EncodeController : Controller {
        private IEncoderRepository _repo;
        public EncodeController(IEncoderRepository repo) {
            this._repo = repo;
        }

        /// <summary>
        /// Responds to HTTP GET by converting given openText using selected encoder.
        /// 
        /// Expected request parameters:
        /// encoder: Name of the encoder.
        /// openText: Text to be encoded.
        /// </summary>
        /// <returns>JSON representation of encoding result. See ControllerExtensions.ToJson() for response details.</returns>
        [HttpGet]
        public JsonResult GetAll() {
            var encoderId = HttpContext.Request.Query["encoder"];
            var encoder = this._repo.GetEncoder(encoderId);
            if (encoder != null) {
                var openText = HttpContext.Request.Query["openText"];
                var encodeResult = encoder.Encode(openText);
                return encodeResult.ToJson();
            } else {
                return EncodingResult.NoSuchEncoder.ToJson();
            }
        }
    }
}
