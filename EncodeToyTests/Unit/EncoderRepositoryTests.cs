using EncodeToy.Models;
using System;
using System.Collections.Generic;
using Xunit;
using Moq;

namespace EncodeToyTests.Unit {
    public class EncoderRepositoryTests {
        [Fact]
        public void NamesReturnsListOfNames() {
            // arrange
            var encoderMock = new Mock<IEncoder>();
            var namedFactoryMock = new Mock<INamedEncoderFactory>();
            namedFactoryMock.Setup(f => f.Name).Returns("name");
            namedFactoryMock.Setup(f => f.Create()).Returns(encoderMock.Object);

            // act
            var repo = new EncoderRepository(new[] { namedFactoryMock.Object });

            // assert
            Assert.Equal(new[] { "name" }, repo.Names);
        }

        [Fact]
        public void GetReturnsEncoder() {
            // arrange
            var encoderMock = new Mock<IEncoder>();
            var namedFactoryMock = new Mock<INamedEncoderFactory>();
            namedFactoryMock.Setup(f => f.Name).Returns("name");
            namedFactoryMock.Setup(f => f.Create()).Returns(encoderMock.Object);

            // act
            var repo = new EncoderRepository(new[] { namedFactoryMock.Object });

            // assert
            Assert.Same(encoderMock.Object, repo.GetEncoder("name"));
        }
    }
}
