using EncodeToy.Models;
using System;
using Xunit;

namespace EncodeToyTests.Unit {
    public class MorseEncoderTests {
        [Theory]
        [InlineData("a", ".-")]
        [InlineData("A", ".-")]
        [InlineData("ab", ".-/-...")]
        [InlineData("AB", ".-/-...")]
        [InlineData("\"THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.\"(0!1_2-3@4/5=6,7;8?9)", ".-..-./-/...././#/--.-/..-/../-.-./-.-/#/-.../.-./---/.--/-./#/..-./---/-..-/#/.---/..-/--/.--./.../#/---/...-/./.-./#/-/...././#/.-../.-/--../-.--/#/-../---/--./.-.-.-/.-..-./-.--./-----/--...-/.----/..--.-/..---/-....-/...--/.--.-./....-/-..-./...../-...-/-..../--..--/--.../-.-.-./---../..--../----./-.--.-")]
        [InlineData("~", "#")]
        public void EncodingReturnsProperResult(string open, string closed) {
            // arrange
            var encoder = new MorseEncoder();

            // act
            var result = encoder.Encode(open);

            // assert
            Assert.Equal(Result.OK, result.Result);
            Assert.Equal(closed, result.Data);
            Assert.Equal(null, result.ErrorMessage);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void EncodingInvalidInputReturnsError(string open) {
            // arrange
            var encoder = new MorseEncoder();

            // act
            var result = encoder.Encode(open);

            // assert
            Assert.Equal(Result.Error, result.Result);
            Assert.Equal(open, result.Data);
            Assert.Equal("Open text is empty.", result.ErrorMessage);
        }


        [Theory]
        [InlineData(".-", "A")]
        [InlineData(".-/-...", "AB")]
        [InlineData(".-..-./-/...././#/--.-/..-/../-.-./-.-/#/-.../.-./---/.--/-./#/..-./---/-..-/#/.---/..-/--/.--./.../#/---/...-/./.-./#/-/...././#/.-../.-/--../-.--/#/-../---/--./.-.-.-/.-..-./-.--./-----/--...-/.----/..--.-/..---/-....-/...--/.--.-./....-/-..-./...../-...-/-..../--..--/--.../-.-.-./---../..--../----./-.--.-", "\"THE#QUICK#BROWN#FOX#JUMPS#OVER#THE#LAZY#DOG.\"(0!1_2-3@4/5=6,7;8?9)")]
        [InlineData("#", "#")]
        public void DencodingReturnsProperResult(string closed, string open) {
            // arrange
            var encoder = new MorseEncoder();

            // act
            var result = encoder.Decode(closed);

            // assert
            Assert.Equal(Result.OK, result.Result);
            Assert.Equal(open, result.Data);
            Assert.Equal(null, result.ErrorMessage);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void DencodingInvalidInputReturnsError(string closed) {
            // arrange
            var encoder = new MorseEncoder();

            // act
            var result = encoder.Decode(closed);

            // assert
            Assert.Equal(Result.Error, result.Result);
            Assert.Equal(closed, result.Data);
            Assert.Equal("Closed text is empty.", result.ErrorMessage);
        }
    }
}
