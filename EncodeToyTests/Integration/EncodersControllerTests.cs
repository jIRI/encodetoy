using EncodeToy;
using EncodeToy.Controllers;
using EncodeToy.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.TestHost;
using Moq;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace EncodeToyTests.Integration {
    public class EncodersControllerTests : IntegrationTest {
        [Fact]
        public async Task EncodingValidInputReturnsProperResult() {
            // Act
            var response = await this.Client.GetAsync($"/api/encoders");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<RequestResponseEncoders>(responseString);

            // Assert
            Assert.Equal(new[] { "morse" }, data.encoders);
        }
    }
}
