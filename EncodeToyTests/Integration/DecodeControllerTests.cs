using EncodeToy;
using EncodeToy.Controllers;
using EncodeToy.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.TestHost;
using Moq;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace EncodeToyTests.Integration {
    public class DecodeControllerTests : IntegrationTest {
        [Theory]
        [InlineData("encoder=morse&closedText=.-", "A")]
        [InlineData("encoder=morse&closedText=.-/-...", "AB")]
        [InlineData("encoder=morse&closedText=*", "#")]
        public async Task DecodingValidInputReturnsProperResult(string queryString, string expectedResponseString) {
            // Act
            var response = await this.Client.GetAsync($"/api/decode?{queryString}");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<RequestResponseEncodeDecode>(responseString);

            // Assert
            Assert.Equal(Result.OK.ToString(), data.status);
            Assert.Equal(expectedResponseString, data.result);
        }

        [Theory]
        [InlineData("encoder=morse&closedText=", "Closed text is empty.")]
        [InlineData("encoder=morse", "Closed text is empty.")]
        [InlineData("encoder=Q&closedText=a", "No such encoder.")]
        public async Task DecodingInvalidInputReturnsError(string queryString, string errorMessage) {
            // Act
            var response = await this.Client.GetAsync($"/api/decode?{queryString}");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<RequestResponseEncodeDecode>(responseString);

            // Assert
            Assert.Equal(Result.Error.ToString(), data.status);
            Assert.Equal(errorMessage, data.error);
        }
    }
}
