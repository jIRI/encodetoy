using EncodeToy;
using EncodeToy.Controllers;
using EncodeToy.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.TestHost;
using Moq;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace EncodeToyTests.Integration {
    public class EncodeControllerTests : IntegrationTest {
        [Theory]
        [InlineData("encoder=morse&openText=a", ".-")]
        [InlineData("encoder=morse&openText=aB", ".-/-...")]
        [InlineData("encoder=morse&openText=*", "#")]
        public async Task EncodingValidInputReturnsProperResult(string queryString, string expectedResponseString) {
            // Act
            var response = await this.Client.GetAsync($"/api/encode?{queryString}");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<RequestResponseEncodeDecode>(responseString);

            // Assert
            Assert.Equal(Result.OK.ToString(), data.status);
            Assert.Equal(expectedResponseString, data.result);
        }

        [Theory]
        [InlineData("encoder=morse&openText=", "Open text is empty.")]
        [InlineData("encoder=morse", "Open text is empty.")]
        [InlineData("encoder=Q&openText=a", "No such encoder.")]
        public async Task EncodingInvalidInputReturnsError(string queryString, string errorMessage) {
            // Act
            var response = await this.Client.GetAsync($"/api/encode?{queryString}");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<RequestResponseEncodeDecode>(responseString);

            // Assert
            Assert.Equal(Result.Error.ToString(), data.status);
            Assert.Equal(errorMessage, data.error);
        }
    }
}
