﻿using EncodeToy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace EncodeToyTests {
    /// <summary>
    /// Represents deserialized JSON response from API.
    /// Used to check result of en/decoding API call.
    /// </summary>
    public class RequestResponseEncodeDecode {
        public string status { get; set; }
        public string error { get; set; }
        public string result { get; set; }
    }

    /// <summary>
    /// Represents deserialized JSON response to request for list of encoders.
    /// </summary>
    public class RequestResponseEncoders {
        public string[] encoders { get; set; }
    }

    /// <summary>
    /// Base class for integration tests.
    /// </summary>
    public class IntegrationTest {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        public IntegrationTest() {
            // Arrange                
            this._server = new TestServer(new WebHostBuilder()
                                .UseContentRoot(TestHelpers.AppRootDir)
                                .UseStartup<Startup>()
                            );
            this._client = _server.CreateClient();
        }

        public TestServer Server => this._server;
        public HttpClient Client => this._client;
    }

    public static class TestHelpers {
        /// <summary>
        /// Directory of the app project.
        /// Used to start test app for integration testing.
        /// </summary>
        public static string AppRootDir {
            get {
                return System.IO.Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\..\\EncodeToy"); ;
            }
        }
    }
}
